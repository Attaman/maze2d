﻿using System;
using Common.DataStructures;
using Maze.Controller;
using Maze.GameState.Model;
using UnityEngine;

namespace Maze.GameState.Controller
{
    public class GameStateController : MonoBehaviour
    {
        public event Action GameComplete;

        public float CurrentGameTime
        {
            get
            {
                return (_isFinished ? _timeFinish : Time.timeSinceLevelLoad) - _timeStart;
            }
        }

        public float DistanceCovered { get; private set; }

        [SerializeField] private GenerationController _generationController;
        [SerializeField] private PlayerController _playerController;
        private PlayerController _playerInstance;

        private bool _isFinished;
        private float _timeStart;
        private float _timeFinish;
        private Vector2[] _exitPositions;

        public void NewGame(GameConfig config)
        {
            DistanceCovered = 0;
            _isFinished = false;

            _generationController.Generate(
                config.SizeX,
                config.SizeY,
                config.ExitsCount,
                out Vector3 startPosition,
                out _exitPositions
            );

            if (_playerInstance == null)
            {
                _playerInstance = Instantiate(_playerController);
                _playerInstance.transform.position = startPosition;

                _playerInstance.PlayerMoved += OnPlayerMoved;
            }
            else
            {
                _playerInstance.Restart();
            }

            _timeStart = Time.timeSinceLevelLoad;
        }

        private void OnPlayerMoved(Vector3 direction)
        {
            DistanceCovered += direction.magnitude;
            for (int i = 0, length = _exitPositions.Length; i < length; i++)
            {
                Vector3 position = _exitPositions[i];
                if (Vector3.Distance(_playerInstance.transform.position, position) < 1)
                {
                    CompleteGame();
                }
            }
        }

        private void CompleteGame()
        {
            _playerInstance.Stop();

            _timeFinish = Time.timeSinceLevelLoad;
            _isFinished = true;

            if (SaveManager.GetBestTime() > CurrentGameTime)
            {
                SaveManager.SetBestTime(CurrentGameTime);
            }

            if (SaveManager.GetBestDistance() > DistanceCovered)
            {
                SaveManager.SetBestDistance(DistanceCovered);
            }

            GameComplete?.Invoke();
        }
    }
}
