﻿using System;

namespace Maze.GameState.Model
{
    [Serializable]
    public struct GameConfig
    {
        public int SizeX;
        public int SizeY;
        public int ExitsCount;

        public GameConfig(int sizeX, int sizeY, int exitsCount)
        {
            SizeX = sizeX;
            SizeY = sizeY;
            ExitsCount = exitsCount;
        }
    }
}
