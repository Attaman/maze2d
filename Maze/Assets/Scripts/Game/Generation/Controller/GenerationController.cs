﻿using System;
using Maze.Generation.View;
using Maze.Generation.Model;
using UnityEngine;
using Common.DataStructures;

namespace Maze.Controller
{
    public class GenerationController : MonoBehaviour
    {
        [SerializeField] private AbstractMazePresentation _mazePresentation;

        private MazeModel _model;

        public void Generate(int sizeX, int sizeY, int exitsCount, out Vector3 startPosition, out Vector2[] exitPositions)
        {
            _model = new MazeModel(sizeX, sizeY, exitsCount);
            int seed = (int)((DateTimeOffset)DateTime.Now).ToUnixTimeSeconds();
            //for debug purposes
            Debug.Log(seed);

            Int2[] exits = _model.Generate(seed);
            _mazePresentation.Init(_model.Matrix);

            Int2 startCell = GetStartCell();
            startPosition = _mazePresentation.GetWorldPosition(new Vector3Int(startCell.X, startCell.Y, 0));

            exitPositions = new Vector2[exits.Length];
            for (int i = 0; i < exits.Length; i++)
            {
                Int2 exit = exits[i];

                exitPositions[i] = _mazePresentation.GetWorldPosition(new Vector3Int(exit.X, exit.Y, 0));
            }
        }

        private Int2 GetStartCell()
        {
            int halfSizeX = _model.Matrix.Size.X / 2;
            int halfSizeY = _model.Matrix.Size.Y / 2;
            for (int i = halfSizeX + UnityEngine.Random.Range(-halfSizeX / 2, halfSizeX / 2), iCounter = 0; iCounter < 2; iCounter++)
            {
                for (int j = halfSizeY + UnityEngine.Random.Range(-halfSizeY / 2, halfSizeY / 2), jCounter = 0; jCounter < 2; jCounter++)
                {
                    if (_model.Matrix.GetCell(i + iCounter, j + jCounter).IsWalkable)
                    {
                        return new Int2(i + iCounter, j + jCounter);
                    }
                }
            }

            return default;
        }
    }
}
