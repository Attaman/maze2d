﻿using System;
using Maze.Generation.Model;
using UnityEngine.Tilemaps;

namespace Maze.Generation.View
{
    [Serializable]
    public struct TileEntryDescription
    {
        public Binom Type;
        public TileBase Tile;
    }
}
