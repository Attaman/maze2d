﻿using System.Collections.Generic;
using UnityEngine;

namespace Maze.Generation.View
{
    [CreateAssetMenu(fileName = "TileDatabase", menuName = "Maze/Generation/TileDatabase")]
    public class TilesDatabase : ScriptableObject
    {
        public List<TileEntryDescription> Tiles;
    }
}
