﻿using Common.DataStructures;
using Maze.Generation.Model;
using UnityEngine;

namespace Maze.Generation.View
{
    public abstract class AbstractMazePresentation : MonoBehaviour, IMazePresentation
    {
        public abstract Vector3 GetWorldPosition(Vector3Int localCell);
        public abstract void Init(MazeMatrix mazeMatrix);
    }
}
