﻿using Common.DataStructures;
using Maze.Generation.Model;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Maze.Generation.View
{
    public class Maze2DPresentation : AbstractMazePresentation
    {
        [SerializeField] private Tilemap _mazeTileMap;
        [SerializeField] private Tilemap _groundTileMap;
        [SerializeField] private TilesDatabase _tilesDatabase;

        public override Vector3 GetWorldPosition(Vector3Int localCell)
        {
            return _mazeTileMap.CellToWorld(localCell);
        }

        public override void Init(MazeMatrix mazeMatrix)
        {
            _mazeTileMap.size = new Vector3Int(mazeMatrix.Size.X, mazeMatrix.Size.Y, 0);
            _mazeTileMap.ClearAllTiles();

            _groundTileMap.size = new Vector3Int(mazeMatrix.Size.X, mazeMatrix.Size.Y, 0);
            _groundTileMap.ClearAllTiles();

            for (int i = 0; i < mazeMatrix.Size.X; i++)
            {
                for (int j = 0; j < mazeMatrix.Size.Y; j++)
                {
                    Cell cell = mazeMatrix.GetCell(i, j);

                    if (!cell.IsWalkable)
                    {
                        _mazeTileMap.SetTile(new Vector3Int(i, j, 0), GetTile(cell.Type));
                    }
                    else
                    {
                        _groundTileMap.SetTile(new Vector3Int(i, j, 0), GetTile(cell.Type));
                    }
                }
            }

            transform.position = -_mazeTileMap.cellBounds.center;
        }

        /*
         * TODO: this logic can be optimized by dictionary and can be improved for better visual like chosing
         * random tile from all compatable or calculating better tile depend on neightbout tiles
        */
        private TileBase GetTile(Binom type)
        {
            for (int i = 0, count = _tilesDatabase.Tiles.Count; i < count; i++)
            {
                var tileEntry = _tilesDatabase.Tiles[i];
                if (tileEntry.Type == type)
                {
                    return tileEntry.Tile;
                }
            }

            Debug.LogErrorFormat("Missing tile for binome {0}", type);
            return null;
        }
    }
}
