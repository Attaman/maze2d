﻿using Common.DataStructures;
using Maze.Generation.Model;
using UnityEngine;

namespace Maze.Generation.View
{
    public interface IMazePresentation
    {
        void Init(MazeMatrix mazeMatrix);
        Vector3 GetWorldPosition(Vector3Int localCell);
    }
}
