﻿namespace Maze.Generation.Model
{
    public struct Cell
    {
        public bool IsWalkable;
        public Binom Type;
    }
}