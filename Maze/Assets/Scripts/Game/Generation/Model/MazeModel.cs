﻿using System;
using System.Collections.Generic;
using Common.DataStructures;
using UnityEngine;

namespace Maze.Generation.Model
{
    public class MazeModel
    {
        private const float ExitOffsetRandomizationCoef = 0.8f;
        private const int MinSize = 4;
        public MazeMatrix Matrix => _matrix;

        private readonly MazeMatrix _matrix;
        private readonly int _exitsCount;
        private System.Random _random;

        public MazeModel(int sizeX, int sizeY, int exitsCount)
        {
            if (sizeX < MinSize || sizeY < MinSize)
            {
                throw new Exception("Incorrect maze size");
            }

            if (exitsCount > Math.Min(sizeX, sizeY) || exitsCount < 1)
            {
                throw new Exception("Exits count is wrong");
            }
            int matrixSizeX = sizeX * 2 + 1;
            int matrixSizeY = sizeY * 2 + 1;
            _matrix = new MazeMatrix(matrixSizeX, matrixSizeY);
            _exitsCount = exitsCount;
        }

        public Int2[] Generate(int seed)
        {
            _random = new System.Random(seed);

            Int2[] borderCellPositions = GetBorderCellPositions();
            InitBorderWalls(in borderCellPositions);

            Int2[] exitCellPositions = GetExits(in borderCellPositions);
            InitExits(exitCellPositions);

            Int2[,] visited = new Int2[_matrix.Size.X, _matrix.Size.Y];

            Int2[] neighboursBuffer = new Int2[4];
            List<int> nextIndeces = new List<int>();
            
            //TODO: can calculate start capacity to avoid realocations
            List<Int2> currentPostions = new List<Int2> ();
            List<Int2> newPositions = new List<Int2> ();

            newPositions.Add(exitCellPositions[0]);

            while (newPositions.Count > 0)
            {
                currentPostions.Clear();

                for (int i = 0; i < newPositions.Count; i++)
                {
                    currentPostions.Add(newPositions[i]);
                }

                newPositions.Clear();

                for (int i = 0; i < currentPostions.Count; i++)
                {
                    Int2 postion = currentPostions[i];
                    int neighboursCount = GetNeighbourCells(postion, neighboursBuffer, Binom.None, 1);

                    if (neighboursCount > 0)
                    {
                        nextIndeces.Clear();
                        int newCount = _random.Next(1, neighboursCount);

                        //chosing neihbours to go
                        for (int j = 0; j < newCount; j++)
                        {
                            int newIndex = _random.Next(0, neighboursCount);
                            if (!nextIndeces.Contains(newIndex))
                            {
                                nextIndeces.Add(newIndex);
                            }
                        }

                        //mark neighbours to go as walked
                        for (int j = 0; j < nextIndeces.Count; j++)
                        {
                            Int2 next = neighboursBuffer[nextIndeces[j]];

                            newPositions.Add(next);
                            _matrix.SetCell(next.X, next.Y, new Cell() { IsWalkable = true, Type = Binom.Ground });
                        }

                        //markother neighbours as wall
                        for (int j = 0; j < neighboursCount; j++)
                        {
                            if (!nextIndeces.Contains(j))
                            {
                                Int2 position = neighboursBuffer[j];
                                _matrix.SetCell(position.X, position.Y, new Cell() { IsWalkable = false, Type = Binom.Wall });
                            }
                        }
                    }
                }

                if (newPositions.Count == 0 && FindUnvisited(out Int2 unvisited))
                {         
                    _matrix.SetCell(unvisited.X, unvisited.Y, new Cell() { IsWalkable = true, Type = Binom.Ground });
                    newPositions.Add(unvisited);

                    int neighboursCount = GetNeighbourCells(unvisited, neighboursBuffer, Binom.Ground, 2);
                    if (neighboursCount > 0)
                    {
                        RemoveWall(unvisited, neighboursBuffer[_random.Next(0, neighboursCount)]);
                    }
                }
            }

            return exitCellPositions;
        }

        private void RemoveWall(Int2 first, Int2 second)
        {
            int xDiff = second.X - first.X;
            int yDiff = second.Y - first.Y;
            int addX, addY;

            addX = (xDiff != 0) ? (xDiff / Math.Abs(xDiff)) : 0;
            addY = (yDiff != 0) ? (yDiff / Math.Abs(yDiff)) : 0;
            _matrix.SetCell(first.X + addX, first.Y + addY, new Cell() { IsWalkable = true, Type = Binom.Ground });
        }

        //TODO: pretty costly method, maybe need to find better way of detecting unvisited cells
        private bool FindUnvisited(out Int2 unvisited)
        {
            unvisited = default;
            for (int i = 0; i < _matrix.Size.X; i++)
            {
                for (int j = 0; j < _matrix.Size.Y; j++)
                {
                    if (_matrix.GetCell(i, j).Type == Binom.None)
                    {
                        unvisited = new Int2(i, j);
                        return true;
                    }
                }
            }

            return false;
        }

        private int GetNeighbourCells(Int2 position, Int2[] buffer, Binom type, int range)
        {
            int counter = 0;

            if (position.X - range >= 0 && _matrix.GetCell(position.X - range, position.Y).Type == type)
            {
                buffer[counter++] = new Int2(position.X - range, position.Y);
            }

            if (position.X + range < _matrix.Size.X && _matrix.GetCell(position.X + range, position.Y).Type == type)
            {
                buffer[counter++] = new Int2(position.X + range, position.Y);
            }

            if (position.Y + range < _matrix.Size.Y && _matrix.GetCell(position.X, position.Y + range).Type == type)
            {
                buffer[counter++] = new Int2(position.X, position.Y + range);
            }

            if (position.Y - range >= 0 && _matrix.GetCell(position.X, position.Y - range).Type == type)
            {
                buffer[counter++] = new Int2(position.X, position.Y - range);
            }

            return counter;
        }

        private Int2[] GetBorderCellPositions()
        {
            //here are no magic numbers, precalculated expressions
            Int2 size = _matrix.Size;

            int borderLength = (size.X + size.Y - 2) * 2;
            Int2[] positions = new Int2[borderLength];
            int counter = 0;

            for (int i = 0; i < size.X; i++)
            {
                if (i % 2 != 0)
                {
                    positions[i] = new Int2(i, 0);
                    positions[i + size.X + size.Y - 2] = new Int2(size.X - i - 1, size.Y - 1);
                    counter += 2;
                }
            }

            for (int i = 0; i < size.Y; i++)
            {
                if (i % 2 != 0)
                {
                    int initOffset = i + size.X - 1;
                    positions[initOffset] = new Int2(size.X - 1, i);
                    int additionalOffset = initOffset + size.Y + size.X - 2;
                    if (additionalOffset < positions.Length)
                    {
                        positions[additionalOffset] = new Int2(0, size.Y - i - 1);
                        counter += 2;
                    }
                }
            }

            Int2[] newPositions = new Int2[counter];

            for (int i = 0; i < positions.Length; i++)
            {
                if(i % 2 != 0)
                {
                    newPositions[i / 2] = positions[i];
                }
            }
            return newPositions;
        }

        private Int2[] GetExits(in Int2[] borderCellPositions)
        {
            int count = borderCellPositions.Length;

            int exitsCount = _exitsCount;

            int exitOffset = count / exitsCount;
            Int2[] positions = new Int2[exitsCount];

            int previousInBorder = _random.Next(0, count);
            positions[0] = borderCellPositions[previousInBorder];

            int sign = _random.Next(-1, 1) < 0 ? -1 : 1;
            int offsetCompensation = 0;

            for (int i = 1; i < exitsCount; i++)
            {
                int maxOffset = exitOffset + offsetCompensation;
                int offset = _random.Next((int)(maxOffset * ExitOffsetRandomizationCoef), maxOffset);
                offsetCompensation = maxOffset - offset;

                previousInBorder += sign * offset;

                if (previousInBorder < 0)
                {
                    previousInBorder += count;
                }
                else if (previousInBorder >= count)
                {
                    previousInBorder -= count;
                }

                positions[i] = borderCellPositions[previousInBorder];
            }

            return positions;
        }

        private void InitBorderWalls(in Int2[] borderCellPositions)
        {
            for (int i = 0, length = borderCellPositions.Length; i < length; i++)
            {
                Int2 cellPosition = borderCellPositions[i];
                _matrix.SetCell(cellPosition.X, cellPosition.Y, new Cell() { IsWalkable = false, Type = Binom.Wall });
            }
        }

        private void InitExits(Int2[] exitPositions)
        {
            for (int i = 0, length = exitPositions.Length; i < length; i++)
            {
                Int2 cellPosition = exitPositions[i];
                _matrix.SetCell(cellPosition.X, cellPosition.Y, new Cell() { IsWalkable = true, Type = Binom.Ground });

                //for easier calculations for generation algorithm
                Int2 additionalCell = cellPosition;
                if (cellPosition.X == 0)
                {
                    additionalCell.X += 1;
                }
                else if (cellPosition.X == _matrix.Size.X - 1)
                {
                    additionalCell.X -= 1;
                }

                if (cellPosition.Y == 0)
                {
                    additionalCell.Y += 1;
                }
                else if (cellPosition.Y == _matrix.Size.Y - 1)
                {
                    additionalCell.Y -= 1;
                }

                _matrix.SetCell(additionalCell.X, additionalCell.Y, new Cell() { IsWalkable = true, Type = Binom.Ground });

                exitPositions[i] = additionalCell;
            }
        }
    }
}
