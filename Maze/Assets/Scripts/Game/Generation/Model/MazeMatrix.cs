﻿using Common.DataStructures;

namespace Maze.Generation.Model
{
    public struct MazeMatrix
    {
        public Int2 Size => _size;

        private readonly Int2 _size;
        private readonly Cell[,] _cells;

        public MazeMatrix(int sizeX, int sizeY)
        {
            _size = new Int2() { X = sizeX, Y = sizeY };
            _cells = new Cell[sizeX, sizeY];

            for (int i = 0; i < sizeX; i++)
            {
                for (int j = 0; j < sizeY; j++)
                {
                    if ((i % 2 != 0 || j % 2 != 0 && (i + j) % 2 != 0) && (i < sizeX - 1 && j < sizeY - 1))
                    {
                        _cells[i, j].IsWalkable = true;
                    }
                    else
                    {
                        _cells[i, j].Type = Binom.Wall;
                    }
                }
            }
        }

        public Cell GetCell(int x, int y)
        {
            return _cells[x, y];
        }

        public void SetCell(int x, int y, Cell cell)
        {
            _cells[x, y] = cell;
        }
    }
}
