﻿using System;
using UnityEngine;

public static class SaveManager
{
    private static readonly string BestTimeKeyHash = "bestTime".GetHashCode().ToString();
    private static readonly string BestDistanceKeyHash = "bestDistance".GetHashCode().ToString();

    public static float GetBestTime()
    {
        return PlayerPrefs.GetFloat(BestTimeKeyHash, float.MaxValue);
    }

    public static float GetBestDistance()
    {
        return PlayerPrefs.GetFloat(BestDistanceKeyHash, float.MaxValue);
    }

    public static void SetBestTime(float time)
    {
        PlayerPrefs.SetFloat(BestTimeKeyHash, time);
        PlayerPrefs.Save();
    }

    public static void SetBestDistance(float distance)
    {
        PlayerPrefs.SetFloat(BestDistanceKeyHash, distance);
        PlayerPrefs.Save();
    }
}
