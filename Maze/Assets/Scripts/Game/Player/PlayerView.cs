﻿using System;
using UnityEngine;

public class PlayerView : MonoBehaviour
{
    public void LookAt(Vector3 mouseWorldPosition)
    {
        Vector3 direction = mouseWorldPosition - transform.position;
        transform.rotation = Quaternion.LookRotation(Vector3.forward, direction);
    }
}
