﻿using System;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public event Action<Vector3> PlayerMoved;

    [SerializeField] private float _size;
    [SerializeField] private float _speed;
    [SerializeField] private PlayerView view;

    private Camera _camera;

    private void Start()
    {
        _camera = Camera.main;
    }

    private void Update()
    {
        Vector3 direction = Vector3.zero;

        if (Input.GetKey(KeyCode.W))
        {
            direction.y = 1;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            direction.y = -1;
        }

        if (Input.GetKey(KeyCode.A))
        {
            direction.x = -1;

        }
        else if (Input.GetKey(KeyCode.D))
        {
            direction.x = 1;
        }

        var mousePosition = Input.mousePosition;
        Vector3 mouseWorldPosition = _camera.ScreenToWorldPoint(new Vector3(mousePosition.x, mousePosition.y, 5));
        view.LookAt(mouseWorldPosition);

        if (direction != Vector3.zero)
        {
            direction = direction.normalized;

            RaycastHit2D raycastHit = Physics2D.CircleCast(transform.position, _size, direction, _speed * Time.deltaTime);
            direction *= raycastHit.collider != null ? (raycastHit.distance - 0.01f) : _speed * Time.deltaTime;

            transform.Translate(direction);
            PlayerMoved?.Invoke(direction);
        }
    }

    public void Stop()
    {
        enabled = false;
    }

    public void Restart()
    {
        enabled = true;
    }
}
