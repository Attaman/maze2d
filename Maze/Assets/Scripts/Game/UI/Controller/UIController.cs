﻿using System;
using Maze.GameState.Controller;
using Maze.GameState.Model;
using Maze.UI.View;
using UnityEngine;

namespace Maze.UI.Controller
{
    public class UIController: MonoBehaviour
    {
        [SerializeField] private GameStateController _gameStateController;
        [SerializeField] private HUDView _hudView;
        [SerializeField] private GameConfigurationView _configurationView;
        [SerializeField] private GameEndView _gameEndView;

        private void Start()
        {
            _configurationView.SetEnabled(true);
            _hudView.SetEnabled(false);
            _gameEndView.SetEnabled(false);

            _configurationView.StartPressed += StartPressed;
            _gameStateController.GameComplete += OnGameComplete;
            _gameEndView.RestartPressed += OnRestart;
        }

        private void OnRestart()
        {
            _configurationView.SetEnabled(true);
            _gameEndView.SetEnabled(false);
        }

        private void StartPressed(GameConfig config)
        {
            _hudView.SetEnabled(true);
            _configurationView.SetEnabled(false);
            _gameStateController.NewGame(config);
        }

        private void Update()
        {
            _hudView.SetTime(_gameStateController.CurrentGameTime);
            _hudView.SetDistance(_gameStateController.DistanceCovered);
        }

        private void OnGameComplete()
        {
            _hudView.SetEnabled(false);
            _gameEndView.SetEnabled(true);

            _gameEndView.SetResult(
                _gameStateController.CurrentGameTime,
                SaveManager.GetBestTime(),
                _gameStateController.DistanceCovered,
                SaveManager.GetBestDistance()
            );
        }
    }
}
