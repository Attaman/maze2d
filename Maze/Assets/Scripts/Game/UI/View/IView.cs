﻿namespace Maze.UI.View
{
    public interface IView
    {
        void SetEnabled(bool isEnabled);
    }
}
