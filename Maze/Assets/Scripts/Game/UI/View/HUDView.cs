﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Maze.UI.View
{
    public class HUDView : MonoBehaviour, IView
    {
        [SerializeField] private TMP_Text _timer;
        [SerializeField] private TMP_Text _distanceCovered;

        public void SetEnabled(bool isEnabled)
        {
            gameObject.SetActive(isEnabled);
        }

        public void SetTime(float time)
        {
            _timer.text = time.ToString("0.0");
        }

        public void SetDistance(float distance)
        {
            _distanceCovered.text = distance.ToString("0");
        }
    }
}
