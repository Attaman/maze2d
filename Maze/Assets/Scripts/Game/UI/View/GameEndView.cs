﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Maze.UI.View
{
    public class GameEndView : MonoBehaviour, IView
    {
        public event Action RestartPressed;

        [SerializeField] private TMP_Text _timer;
        [SerializeField] private TMP_Text _distanceCovered;

        [SerializeField] private TMP_Text _bestDistance;
        [SerializeField] private TMP_Text _bestTime;
        [SerializeField] private Button _restartButton;

        private void Start()
        {
            _restartButton.onClick.AddListener(RestartPressed.Invoke);
        }

        public void SetEnabled(bool isEnabled)
        {
            gameObject.SetActive(isEnabled);
        }

        public void SetResult(float time, float bestTime, float distance, float bestDistance)
        {
            _timer.text = time.ToString("0.0");
            _bestTime.text = bestTime.ToString("0.0");

            _distanceCovered.text = distance.ToString("0");
            _bestDistance.text = bestDistance.ToString("0");
        }
    }
}
