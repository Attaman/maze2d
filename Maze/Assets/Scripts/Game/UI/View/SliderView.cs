﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Maze.UI.View
{
    public class SliderView : MonoBehaviour, IView
    {
        public event Action<float> ValueChanged;

        [SerializeField] private TMP_Text _valueLabel;
        [SerializeField] private Slider _slider;

        private void Start()
        {
            _slider.onValueChanged.AddListener(OnValueChanged);
            OnValueChanged(_slider.value);
        }

        public void SetValue(float value)
        {
            _slider.value = value;
        }

        public void SetEnabled(bool isEnabled)
        {
            gameObject.SetActive(isEnabled);
        }

        private void OnValueChanged(float value)
        {
            _valueLabel.text = value.ToString("0");
            ValueChanged?.Invoke(value);
        }
    }
}
