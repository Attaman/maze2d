﻿using System;
using Maze.GameState.Model;
using UnityEngine;
using UnityEngine.UI;

namespace Maze.UI.View
{
    public class GameConfigurationView : MonoBehaviour, IView
    {
        public event Action<GameConfig> StartPressed;

        [SerializeField] private SliderView _sizeX;
        [SerializeField] private SliderView _sizeY;
        [SerializeField] private SliderView _exitsCount;
        [SerializeField] private Button _startButton;
        [SerializeField] private GameConfig _gameConfig;

        private void Start()
        {
            _startButton.onClick.AddListener(OnStart);

            _sizeX.SetValue(_gameConfig.SizeX);
            _sizeY.SetValue(_gameConfig.SizeY);
            _exitsCount.SetValue(_gameConfig.ExitsCount);

            _sizeX.ValueChanged += v => _gameConfig.SizeX = (int)v;
            _sizeY.ValueChanged += v => _gameConfig.SizeY = (int)v;
            _exitsCount.ValueChanged += v => _gameConfig.ExitsCount = (int)v;
        }

        public void SetEnabled(bool isEnabled)
        {
            gameObject.SetActive(isEnabled);
        }

        private void OnStart()
        {
            StartPressed?.Invoke(_gameConfig);
        }
    }
}
