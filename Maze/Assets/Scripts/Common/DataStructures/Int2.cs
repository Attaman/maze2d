﻿namespace Common.DataStructures
{
    public struct Int2
    {
        public int X;
        public int Y;

        public Int2(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }
    }
}
